/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

var DHR = {};
var key;
var county;
var age;

DHR.app = {
    blogData: [],

    eventTemplate: [],
    eventListTemplate: [],

    // Application Constructor
    initialize: function () {
        this.bindEvents();
        this.eventTemplate = Handlebars.compile($("#singleEventPage-template").html());
        this.eventListTemplate = Handlebars.compile($("#eventDisplay-template").html());
        
        county = localStorage.getItem("county");
        age = localStorage.getItem("age");
        
        console.log("INITIALIZED");
        console.log("AGE IN INITIALIZE "+ age);
        
        

    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function () {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function () {
        app.receivedEvent('deviceready');
        //this.get_blog_data();
        
    },

    pageBeforeCreate: function (event, args) {
        //this.get_blog_data();
    },

    get_blog_data: function () {
        var app = this;
        var d = new Date();

        //var today = d.getFullYear()+'-'+ d.getMonth()+1+'-'+d.getDate()+'T00:00:00-01:00';
		var today = d.toISOString();
        console.log("TODAY = "+today);

        var url = 'https://www.googleapis.com/calendar/v3/calendars/mpvndbjrpit95m1skpd55vejn8%40group.calendar.google.com/events?alwaysIncludeEmail=true&orderBy=startTime&singleEvents=true&timeMin='+today;
		key = 'AIzaSyDR5DM4G-wmAcVOWPfc98ZYGGHVeLBAYbY';
        var ua = navigator.userAgent.toLowerCase();
        var isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
        
        if(isAndroid) {
            console.log("ITS AN ANDROID");
            key =  'AIzaSyCEbyu8bPrKBrYnDkn_zLkeQR2YWpv5w0Q';
            }
            
    
    $.ajax({
          url: 'https://www.googleapis.com/calendar/v3/calendars/mpvndbjrpit95m1skpd55vejn8%40group.calendar.google.com/events?alwaysIncludeEmail=true&orderBy=startTime&singleEvents=true&timeMin='+today+'&key='+key,
          type: 'GET',
          dataType: 'json',
          success: function(data) 
          { 
	          	var li = '';
	            DHR.app.blogData.length = 0;
	            for (i in data['items']) {
	                item = data['items'][i];
	
	                DHR.app.blogData.push({
	                    title: item.summary,
	                    description: item.description
	                });
	                //$('#eventDisplay').append({title: item.summary, body: item.description});
	
	            }
	            /*
	            DHR.app.blogData.push({
	                title: "SUCCESS TRIGGERED",
	                description: "FOR NOW"
	            });
	            */
	            //$('#eventDisplay').listview('refresh');
	            //showData(li);
		 		console.log("BEFORE SHOWDATA");
		        showData(DHR.app.eventListTemplate(DHR.app.blogData));
		        console.log("AFTER SHOWDATA");
		        $('#eventListPage').after(DHR.app.eventTemplate(DHR.app.blogData));
        	},
          error: function(data) 
          { 
          	console.log("ERROR GETTING JSON");
            console.log(data);
        	DHR.app.blogData.push({
                    title: data.responseText,
                    description: data.responseText
                });
                
              showData(DHR.app.eventListTemplate(DHR.app.blogData));
        	$('#eventListPage').after(DHR.app.eventTemplate(DHR.app.blogData));
        	
        	
          },
          beforeSend: setHeader
       
      });
     }  ,
      
      
    postBeforeShow: function (event, args) {

        var post = this.blogData[args[1]];
        $("#post-content").html(this.eventTemplate(post));
        $("#post-content").enhanceWithin();


    },
    // Update DOM on a Received Event
    receivedEvent: function (id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    },
    
    
     
};


function setHeader(xhr) {
        
        xhr.setRequestHeader('key', key);
        gapi.client.setApiKey(key);
      }





function displayEducation(age) {
    var html;
    switch (age) {
        case "14":
            html = ['<div data-role="fieldcontain">',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="education-14-1" id="education-1" class="custom" />',
                '<label for="education-1">Begin participating in Life Skills Classes.</label>',
                '</fieldset>',
                ' <fieldset data-role="controlgroup">',
                '<input type="checkbox" name="education-2" id="education-2" class="custom" />',
                ' <label for="education-2">Start getting your community service hours towards graduating high school.</label>',
                '</fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="education-3" id="education-3" class="custom" />',
                '<label for="education-3">Make sure you know your school\'s graduation requirements.</label>',
                '  </fieldset>',
                '  </div>'].join("");
            //console.log(html);
            break;
        case "15":
        case "16":
            html = ['<div data-role="fieldcontain">',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="education-4" id="education-4" class="custom" />',
                '<label for="education-4">Have a plan detailing how you&#39;re going to complete high school or obtain your GED.</label>',
                '</fieldset>',
                ' <fieldset data-role="controlgroup">',
                '<input type="checkbox" name="education-5" id="education-5" class="custom" />',
                ' <label for="education-5">Participate in tutoring services (if necessary)</label>',
                '</fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="education-6" id="education-6" class="custom" />',
                '<label for="education-6">Take a college tour or participate in a vocational/trade training program or school.</label>',
                '  </fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="education-7" id="education-7" class="custom" />',
                '<label for="education-7">Know what is required to be admitted into a college or vocational program or school.</label>',
                '  </fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="education-8" id="education-8" class="custom" />',
                '<label for="education-8">Participate in a Driver&#39;s Education School (if applicable)</label>',
                '  </fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="education-10" id="education-10" class="custom" />',
                '<label for="education-10">Obtain your Learner&#39;s Permit and/or Maryland State Identification Card.</label>',
                '  </fieldset>',
                '  </div>'].join("");
            break;
        case "17":
        case "18":
            html = ['<div data-role="fieldcontain">',
                ' <fieldset data-role="controlgroup">',
                '<input type="checkbox" name="education-11" id="education-11" class="custom" />',
                ' <label for="education-11">	 Have a plan for post-secondary education included in your transitional plan.</label>',
                '</fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="education-12" id="education-12" class="custom" />',
                '<label for="education-12">Complete Free Application For Student Aid (FAFSA).</label>',
                '  </fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="education-13" id="education-13" class="custom" />',
                '<label for="education-13">Enroll in college or a vocational program.</label>',
                '  </fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="education-14" id="education-14" class="custom" />',
                '<label for="education-14">Understand service needs and know where to access services.</label>',
                '  </fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="education-15" id="education-15" class="custom" />',
                '<label for="education-15">Register to vote and understand the voting process.</label>',
                '  </div>'].join("");
            break;
        case "19":
        case "20":
        case "21":
            html = ['<div data-role="fieldcontain">',
                ' <fieldset data-role="controlgroup">',
                '<input type="checkbox" name="education-16" id="education-16" class="custom" />',
                ' <label for="education-16">Have access to post-secondary education supportive services.</label>',
                '  </div>'].join("");
            break;




        default:
            console.log("error in age check");
            console.log(age);
            html = "<div><p>error</p></div>";
    }

    $("#educationContent").empty();
    $("#educationContent").append(html).trigger('create');
    $("#educationContent").enhanceWithin();
    
    loadCheckboxes();

}

function displayHealth(age) {
    var html;
    switch (age) {
        case "14":
            html = ['<div data-role="fieldcontain">',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="health-1" id="health-1" class="custom" />',
                '<label for="health-1">Attend Life Skills Classes and Workshops on healthy relationships, practicing safe sex, forms of birth control, and how to avoid sexually transmitted diseases and infections.</label>',
                '</fieldset>',
                ' <fieldset data-role="controlgroup">',
                '<input type="checkbox" name="health-2" id="health-2" class="custom" />',
                ' <label for="health-2">Understand the causes and effects of drugs and alcohol use.</label>',
                '</fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="health-3" id="health-3" class="custom" />',
                '<label for="health-3">Understand the importance of preventive and routine health care.</label>',
                '  </fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="health-4" id="health-4" class="custom" />',
                '<label for="health-4">Understand the importance of medication and how to use medication properly.</label>',
                '  </fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="health-5" id="health-5" class="custom" />',
                '<label for="health-5">Understand your medical or mental health condition(s) (if applicable). </label>',
                '  </fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="health-6" id="health-6" class="custom" />',
                '<label for="health-6">Identify all of your medical providers (i.e. primary doctor, dentist, mental health).</label>',
                '  </fieldset>',
                '  </div>'].join("");
            //console.log(html);
            break;
        case "15":
        case "16":
            html = ['<div data-role="fieldcontain">',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="health-7" id="health-7" class="custom" />',
                '<label for="health-7">Understand the importance of healthy eating and exercise.</label>',
                '</fieldset>',
                ' <fieldset data-role="controlgroup">',
                '<input type="checkbox" name="health-8" id="health-8" class="custom" />',
                ' <label for="health-8">Know your family medical history and your medical condition.</label>',
                '</fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="health-9" id="health-9" class="custom" />',
                '<label for="health-9">Understand the importance of maintaining your medical records and documentation.</label>',
                '  </fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="health-10" id="health-10" class="custom" />',
                '<label for="health-10">Practice safe and healthy relationships.</label>',
                '  </fieldset>',
                '  </div>'].join("");
            break;
        case "17":
        case "18":
            html = ['<div data-role="fieldcontain">',
                ' <fieldset data-role="controlgroup">',
                '<input type="checkbox" name="health-11" id="health-11" class="custom" />',
                ' <label for="health-11">Understand the importance of medication compliance once you leave care.</label>',
                '</fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="health-12" id="health-12" class="custom" />',
                '<label for="health-12">Be comfortable asking your doctor questions about your medical conditions.</label>',
                '  </fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="health-13" id="health-13" class="custom" />',
                '<label for="health-13">Understand the importance of medical insurance.</label>',
                '  </fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="health-14" id="health-14" class="custom" />',
                '<label for="health-14">Be able to identify and utilize healthcare resources.</label>',
                '  </fieldset>',
                '  </div>'].join("");
            break;
        case "19":
        case "20":
        case "21":
            html = ['<div data-role="fieldcontain">',
            '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="health-15" id="health-15" class="custom" />',
                '<label for="health-15">Be aware of health care coverage options.</label>',
                '  </fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="health-16" id="health-16" class="custom" />',
                '<label for="health-16">Have a copy of medical records and bills prior to leaving care.</label>',
                '  </fieldset>',
                ' <fieldset data-role="controlgroup">',
                '<input type="checkbox" name="health-17" id="health-17" class="custom" />',
                ' <label for="health-17">Understand how to obtain medical services.</label>',
                '  </fieldset>',
                '  </div>'].join("");
            break;




        default:
            console.log("error in age check");
            console.log(age);
            html = "<div><p>error</p></div>";
    }

    $("#healthContent").empty();
    $("#healthContent").append(html).trigger('create');
    $("#healthContent").enhanceWithin();
    
    loadCheckboxes();

}
function displayEmployment(age) {
    var html;
    switch (age) {
        case "14":
            html = ['<div data-role="fieldcontain">',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="employment-1" id="employment-1" class="custom" />',
                '<label for="employment-1">Get a work permit (you may need it to participate in summer employment programs)</label>',
                '</fieldset>',
                ' <fieldset data-role="controlgroup">',
                '<input type="checkbox" name="employment-2" id="employment-2" class="custom" />',
                ' <label for="employment-2">Start exploring various career interests.</label>',
                ' </fieldset>',
                '  </div>'].join("");
            //console.log(html);
            break;
        case "15":
        case "16":
            html = ['<div data-role="fieldcontain">',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="employment-3" id="employment-3" class="custom" />',
                '<label for="employment-3">Receive assistance to explore your career interest in order to understand what skills or trainings are required in the field of interest.</label>',
                '</fieldset>',
                ' <fieldset data-role="controlgroup">',
                '<input type="checkbox" name="employment-4" id="employment-4" class="custom" />',
                ' <label for="employment-4">Understand the minimum wage.</label>',
                '</fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="employment-5" id="employment-5" class="custom" />',
                '<label for="employment-5">Know what jobs are available right now for you to apply to.</label>',
                '  </fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="employment-6" id="employment-6" class="custom" />',
                '<label for="employment-6">Explore summer youth employment programs.</label>',
                '  </fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="employment-7" id="employment-7" class="custom" />',
                '<label for="employment-7">Engage in some type of work experience (i.e. job shadowing, volunteer activities, and/or summer employment). </label>',
                '  </fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="employment-8" id="employment-8" class="custom" />',
                '<label for="employment-8">Understand appropriate attire for job interviews and work dress code.</label>',
                '  </fieldset>',
                '  </div>'].join("");
            break;
        case "17":
        case "18":
            html = ['<div data-role="fieldcontain">',
                ' <fieldset data-role="controlgroup">',
                '<input type="checkbox" name="employment-9" id="employment-9" class="custom" />',
                ' <label for="employment-9">Have a completed resume.</label>',
                '</fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="employment-10" id="employment-10" class="custom" />',
                '<label for="employment-10">Have an understanding of the job market and employment opportunities that you qualify for.</label>',
                '  </fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="employment-11" id="employment-11" class="custom" />',
                '<label for="employment-11">Have at least two summer jobs by age 18.</label>',
                '  </fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="employment-12" id="employment-12" class="custom" />',
                '<label for="employment-12">Have assistance enrolling in occupational training.</label>',
                '  </fieldset>',
                '  </div>'].join("");
            break;
        case "19":
        case "20":
        case "21":
            html = ['<div data-role="fieldcontain">',
            '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="employment-13" id="employment-13" class="custom" />',
                '<label for="employment-13">Maintain employment.</label>',
                '  </fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="employment-14" id="employment-14" class="custom" />',
                '<label for="employment-14">Understand unfair job practices.</label>',
                '  </fieldset>',
                ' <fieldset data-role="controlgroup">',
                '<input type="checkbox" name="employment-15" id="employment-15" class="custom" />',
                ' <label for="employment-15">Have assistance enrolling in occupational training.</label>',
                '  </fieldset>',
                '  </div>'].join("");
            break;

default:
            console.log("error in age check");
            console.log(age);
            html = "<div><p>error</p></div>";
    }

    $("#employmentContent").empty();
    $("#employmentContent").append(html).trigger('create');
    $("#employmentContent").enhanceWithin();
    
    loadCheckboxes();

}

function displayHousing(age) {
    var html;
    switch (age) {
        case "14":
            html = ['<div data-role="fieldcontain">',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="housing-1" id="housing-1" class="custom" />',
                '<label for="housing-1">Live in an approved safe placement with a caring adult.</label>',
                '</fieldset>',
                '  </div>'].join("");
            //console.log(html);
            break;
        case "15":
        case "16":
            html = ['<div data-role="fieldcontain">',
            ' <fieldset data-role="controlgroup">',
                '<input type="checkbox" name="housing-2" id="housing-2" class="custom" />',
                ' <label for="housing-2">Learn how to go grocery shopping, prepare and make simple meals, and safely store food.</label>',
                '</fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="housing-3" id="housing-3" class="custom" />',
                '<label for="housing-3">Know what is essential to cleaning a home and maintaining it. </label>',
                '  </fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="housing-4" id="housing-4" class="custom" />',
                '<label for="housing-4">Understand how to purchase important hygiene products. </label>',
                '  </fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="housing-5" id="housing-5" class="custom" />',
                '<label for="housing-5">Be able to clean clothes and properly care for them (folding your clothes,putting clothes away in proper place, and ironing) </label>',
                '  </fieldset>',
                '  </div>'].join("");
            break;
        case "17":
        case "18":
            html = ['<div data-role="fieldcontain">',
            '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="housing-6" id="housing-6" class="custom" />',
                '<label for="housing-6">Know where you are going to live when you leave foster care.</label>',
                '  </fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="housing-7" id="housing-7" class="custom" />',
                '<label for="housing-7">Understand your rights as a tenant.</label>',
                '</fieldset>',
                ' <fieldset data-role="controlgroup">',
                '<input type="checkbox" name="housing-8" id="housing-8" class="custom" />',
                ' <label for="housing-8">Posses life skills that will allow you to maintain an apartment and make good decisions.</label>',
                '</fieldset>',
                '  </div>'].join("");
            break;
        case "19":
        case "20":
        case "21":
            html = ['<div data-role="fieldcontain">',
            '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="housing-9" id="housing-9" class="custom" />',
                '<label for="housing-9">Be prepared for a Semi Independent Living Arrangement (SILA) or Independent Living Apartment Program.</label>',
                '  </fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="housing-10" id="housing-10" class="custom" />',
                '<label for="housing-10">	Have a budget plan to maintain your household.</label>',
                '  </fieldset>',
                '  </div>'].join("");
            break;




        default:
            console.log("error in age check");
            console.log(age);
            html = "<div><p>error</p></div>";
    }

    $("#housingContent").empty();
    $("#housingContent").append(html).trigger('create');
    $("#housingContent").enhanceWithin();
    
    loadCheckboxes();

}

function displayFinancial(age) {
    var html;
    switch (age) {
        case "14":
            html = ['<div data-role="fieldcontain">',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="financial-1" id="financial-1" class="custom" />',
                '<label for="financial-1">Have a bank account (saving and/or checking)</label>',
                '</fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="financial-2" id="financial-2" class="custom" />',
                '<label for="financial-2">Check your credit report every year.</label>',
                '</fieldset>',
                ' <fieldset data-role="controlgroup">',
                '<input type="checkbox" name="financial-3" id="financial-3" class="custom" />',
                ' <label for="financial-3">Understand basic money management skills.</label>',
                '</fieldset>',
                '  </div>'].join("");
            //console.log(html);
            break;
        case "15":
        case "16":
            html = ['<div data-role="fieldcontain">',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="financial-4" id="financial-4" class="custom" />',
                '<label for="financial-4">Understand the importance of saving.</label>',
                '</fieldset>',
                ' <fieldset data-role="controlgroup">',
                '<input type="checkbox" name="financial-5" id="financial-5" class="custom" />',
                ' <label for="financial-5">Safely and effectively manage your money.</label>',
                '</fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="financial-6" id="financial-6" class="custom" />',
                '<label for="financial-6"> Understand basic financial skills.</label>',
                '  </fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="financial-7" id="financial-7" class="custom" />',
                '<label for="financial-7">Have a budget clothing allowance. </label>',
                '  </fieldset>',
                '  </div>'].join("");
            break;
        case "17":
        case "18":
            html = ['<div data-role="fieldcontain">',
                ' <fieldset data-role="controlgroup">',
                '<input type="checkbox" name="financial-8" id="financial-8" class="custom" />',
                ' <label for="financial-8">Posses basic knowledge of entitlement programs and apply for programs to receive services.</label>',
                '</fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="financial-9" id="financial-9" class="custom" />',
                '<label for="financial-9">Obtain a copy of your credit report.</label>',
                '  </fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="financial-10" id="financial-10" class="custom" />',
                '<label for="financial-10">Understand the benefits of staying in care to the age of 21.</label>',
                '  </fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="financial-11" id="financial-11" class="custom" />',
                '<label for="financial-11">Obtain original or a copy of your birth certificate and social security card.</label>',
                '  </fieldset>',
                '  </div>'].join("");
            break;
        case "19":
        case "20":
        case "21":
            html = ['<div data-role="fieldcontain">',
           ' <fieldset data-role="controlgroup">',
                '<input type="checkbox" name="financial-12" id="financial-12" class="custom" />',
                '<label for="financial-12">Understand the importance of financial investments.</label>',
                '  </fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="financial-13" id="financial-13" class="custom" />',
                '<label for="financial-13">Maintain a savings account.</label>',
                '  </fieldset>',
                ' <fieldset data-role="controlgroup">',
                '<input type="checkbox" name="financial-14" id="financial-14" class="custom" />',
                ' <label for="financial-14">Be able to budget finances to sustain yourselves.</label>',
                '  </fieldset>',
                '  </div>'].join("");
            break;

default:
            console.log("error in age check");
            console.log(age);
            html = "<div><p>error</p></div>";
    }

    $("#financialContent").empty();
    $("#financialContent").append(html).trigger('create');
    $("#financialContent").enhanceWithin();
    
    loadCheckboxes();
}


function displayFamily(age) {
    var html;
    switch (age) {
        case "14":
            html = ['<div data-role="fieldcontain">',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="family-1" id="family-1" class="custom" />',
                '<label for="family-1">Understand positive and safe relationships.</label>',
                '</fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="family-2" id="family-2" class="custom" />',
                '<label for="family-2">Create a photo or scrap book of yourself.</label>',
                '</fieldset>',
                ' <fieldset data-role="controlgroup">',
                '<input type="checkbox" name="family-3" id="family-3" class="custom" />',
                ' <label for="family-3">Create a family tree diagram.</label>',
                '</fieldset>',
                '  </div>'].join("");
            //console.log(html);
            break;
        case "15":
        case "16":
            html = ['<div data-role="fieldcontain">',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="family-4" id="family-4" class="custom" />',
                '<label for="family-4">Be able to identify important and supportive adults.</label>',
                '</fieldset>',
                ' <fieldset data-role="controlgroup">',
                '<input type="checkbox" name="family-5" id="family-5" class="custom" />',
                ' <label for="family-5">Understand the importance of developing life-long relationships.</label>',
                '</fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="family-6" id="family-6" class="custom" />',
                '<label for="family-6"> Practice healthy relations with others. </label>',
                '  </fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="family-7" id="family-7" class="custom" />',
                '<label for="family-7">Use protective measures to avoid sexually transmitted diseases and/ or infections. </label>',
                '  </fieldset>',
                '  </div>'].join("");
            break;
        case "17":
        case "18":
            html = ['<div data-role="fieldcontain">',
                ' <fieldset data-role="controlgroup">',
                '<input type="checkbox" name="family-8" id="family-8" class="custom" />',
                ' <label for="family-8"> Reinforce your family and friend network.</label>',
                '</fieldset>',
                '  </div>'].join("");
            break;
        case "19":
        case "20":
        case "21":
            html = ['<div data-role="fieldcontain">',
            '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="family-9" id="family-9" class="custom" />',
                '<label for="family-9">Learn how to positively develop relationships with family.</label>',
                ' </fieldset>',
                '  </div>'].join("");
            break;

default:
            console.log("error in age check");
            console.log(age);
            html = "<div><p>error</p></div>";
    }

    $("#familyContent").empty();
    $("#familyContent").append(html).trigger('create');
    $("#familyContent").enhanceWithin();
    
    loadCheckboxes();
}


function loadCheckboxes()
{
	$(":checkbox").each(function(){
   
        var thisVar = $(this);
        var id = thisVar.attr('id');
        //var isChecked = thisVar.is(':checked');
        
        var isChecked = localStorage.getItem(id);
         console.log("loading id " + id + "   " + "Checked " + isChecked);
        
        if(isChecked == "true")
        {
        	thisVar.attr("data-cacheval", "false");
        	thisVar.prop("checked", "true");
        	$("label[for="+id+"]").removeClass("ui-checkbox-off");
        	$("label[for="+id+"]").addClass("ui-checkbox-on");
        }
        else
        {
        	thisVar.attr("data-cacheval", "true");
        	//thisVar.prop("checked", "false");
        	$("label[for="+id+"]").removeClass("ui-checkbox-on");
        	$("label[for="+id+"]").addClass("ui-checkbox-off");
        	
        }
        
        //console.log(localStorage.getItem(id));
     
    });
}

function showData(li) {

    $('#eventDisplay').empty();
    $('#eventDisplay').append(li).trigger('create');

    //$('#eventDisplay').append(li);
    $( "#eventDisplay" ).listview().listview('refresh');
}

$(document).on('click', '#eventDisplay li a', function () {
	console.log("EVENT CLICKED");
    $.mobile.changePage("#singleEventPage?", {
        transition: "slide",
        changeHash: true
    });
});


$(document).on('click', '.linkToEventList', function () {
    console.log("EVENTLISTPAGE CLICKED");
    //DHR.app.get_blog_data();
    //$.mobile.changePage("http://mdconnectmylife.org/eventbrite/");
    //window.location.href = "http://mdconnectmylife.org/eventbrite/";
    $.mobile.changePage("#eventbrite");
});

$(document).on('click', '#beginButton', function () {
    
    var today = new Date(),
    dob = new Date($("#dob").val()),
    age = new Date(today - dob).getFullYear()- 1970;
    
    console.log("AGE FROM CALCULATIONS = "+age);
	
	if(age >= 14 && age <=21)
	{
		localStorage.setItem("age", age);
		localStorage.setItem("county", $( "#locationUpdate" ).val());
		localStorage.setItem("initialSetupComplete", "true");
	   
	    
	    $.mobile.pageContainer.pagecontainer("change", "#menu", { transition: "slide",
	        changeHash: true });

   }
   else
   {
   	//console.log("CLICKED BEGIN WITHOUT AGE SET");
   	//localStorage.setItem("age", 14);
   	//$.mobile.pageContainer.pagecontainer("change", "#menu", { transition: "slide",
	//        changeHash: true });

   	//console.log("AGE INPUT ERROR");
   	showError();
   }
});
$(document).on('click', '#saveButton', function () {
    
    var today = new Date(),
    dob = new Date($("#dobUpdate").val()),
    age = new Date(today - dob).getFullYear()- 1970;
    
    console.log("AGE FROM CALCULATIONS = "+age);
	
	if(age >= 14 && age <=21)
	{
		localStorage.setItem("age", age);
		localStorage.setItem("county", $( "#locationUpdate" ).val());
		localStorage.setItem("initialSetupComplete", "true");
	   
	    
	    $.mobile.pageContainer.pagecontainer("change", "#menu", { transition: "slide",
	        changeHash: true });

   }
   else
   {
   //	console.log("CLICKED BEGIN WITHOUT AGE SET");
   //	localStorage.setItem("age", 14);
   //	$.mobile.pageContainer.pagecontainer("change", "#menu", { transition: "slide",
	//        changeHash: true });

   	//console.log("AGE INPUT ERROR");
   	
   	showError();
   }
});

$("#eventListPage").on("pagecontainerbeforeshow", function (event, ui) {
    console.log("PAGEBEFORECHANGE");
    //DHR.app.get_blog_data();// get_blog_data();
});



$("#educationLink").click(function () {
	document.getElementById("educationageHeader").innerHTML= "At age "+localStorage.getItem("age")+", you should:";
    
    displayEducation(localStorage.getItem("age"));

});

$("#employmentLink").click(function () {
	document.getElementById("employmentageHeader").innerHTML= "At age "+localStorage.getItem("age")+", you should:";
    
    displayEmployment(localStorage.getItem("age"));

});

$("#healthLink").click(function () {
	document.getElementById("healthageHeader").innerHTML= "At age "+localStorage.getItem("age")+", you should:";
    
    displayHealth(localStorage.getItem("age"));

});

$("#housingLink").click(function () {
	document.getElementById("housingageHeader").innerHTML= "At age "+localStorage.getItem("age")+", you should:";
    
    displayHousing(localStorage.getItem("age"));

});

$("#financialLink").click(function () {
	document.getElementById("financialageHeader").innerHTML= "At age "+localStorage.getItem("age")+", you should:";
    
    displayFinancial(localStorage.getItem("age"));

});

$("#familyLink").click(function () {
	document.getElementById("familyageHeader").innerHTML= "At age "+localStorage.getItem("age")+", you should:";
    
    displayFamily(localStorage.getItem("age"));

});


function getAge()
{
	return localStorage.getItem("age");
}

function getEmail(county)
{
	var email;
	switch (county) {
        case "allegany":
        	email = "robert.daytonr@maryland.gov";
        	break;
        case "anneArundel":
        	email = "asdfsdaf";
        	break;
        default:
        
        } 
        
        return email;
        	
}

$(document).on('change', ':checkbox', function () {
   
        var thisVar = $(this);
        var id = thisVar.attr('id');
        var isChecked = thisVar.is(':checked');
        console.log("id " + id + "   " + "Checked " + isChecked);
        
        localStorage.setItem(id, isChecked);
        
        //console.log(localStorage.getItem(id));
        
    });
    
$(document).ready(onPageLoad);

function onPageLoad() {
	var initial = "initialPage";
    	console.log(localStorage.getItem("initialSetupComplete"));
        if(localStorage.getItem("initialSetupComplete") == "true")
        {
        	console.log("Should be changing pages");
        	//$.mobile.pageContainer.pagecontainer("change", "shouldBe", { transition: "slide",changeHash: true });
        	initial = 'menu';

	   
        }
        window.location.hash = initial;
        $.mobile.initializePage();
}
    
    
    function showError()
    {
    	$( ".errorMessage" ).show( 200, function() {
      $( this ).text( "Error: Age must be between 14 and 21" );
    });
    $('.errorMessage').delay(5000).fadeOut('slow');
    }
   
